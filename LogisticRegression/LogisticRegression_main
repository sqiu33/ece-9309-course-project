clc;clear;close all
%% Setup the parameters 
input_layer_size  = 120*80;  % 120x80 Input Images of Digits
num_labels = 10;                % 10 labels, from 0 to 9   
                          
%% Prepare the dataset
% Load the digit sample data as an image datastore
digitDatasetPath = fullfile('C:\','Shuwei','Courses','Machine Learning','CourseProject',...
    'datasets','EasternArabicNums','PMU-UD');

imds = imageDatastore(digitDatasetPath,'IncludeSubfolders',true,'LabelSource','foldernames');

%% Specify Training and Validation Sets
%Let the training set contains 60% of all the images
[imdsTrain,imdsValidation] = splitEachLabel(imds,0.6,'randomize');

%Prepare the training data
num_train_imgs = length(imdsTrain.Files);
dataTrain = zeros(num_train_imgs,input_layer_size);
for i = 1:num_train_imgs
   im = imread(imdsTrain.Files{i});
   dataTrain(i,:) = im(:);
end
labelTrain = double(imdsTrain.Labels) - 1;

%Prepare the validation data
num_vali_imgs = length(imdsValidation.Files);
dataVali = zeros(num_vali_imgs,input_layer_size);
for i = 1:num_vali_imgs
   im = imread(imdsValidation.Files{i});
   dataVali(i,:) = im(:);
end
labelVali = double(imdsValidation.Labels) - 1;

%% Training multiple logistic regression classifiers for all the classes
[nImgs_train,nPixels] = size(dataTrain);

thetas = zeros(num_labels,nPixels + 1);%Initialize the coefficients for all classifiers

% Add ones to the data matrix
dataTrain = [ones(nImgs_train, 1), dataTrain];

% Training procedure
lambda = 0.1;
learningRate = 0.1;
maxIte = 1000;
% costs = zeros(num_labels,maxIte);
for j=1:num_labels
    theta = randn(nPixels+1,1);
    
    for jj = 1:maxIte
    [~, grad] = LRcostfun(theta,dataTrain,labelTrain == (j - 1),lambda);
    theta = theta - learningRate*grad;
%     costs(j,jj) = cost;
    end

    thetas(j,:) = theta;
end

%% Validation
num_dataVali = size(dataVali, 1);
dataVali = [ones(num_dataVali, 1) dataVali];

allPredictions = thetas*dataVali';
[maxVal, maxIdx] = max(allPredictions);
prediction = (maxIdx - 1)';

fprintf('\nTraining Set Accuracy: %f\n', sum(prediction == labelVali)/length(labelVali));