%The cost function for logistic regression
%Output:
%cost: the cost when use "theta" as the coefficients of the classifier
%grad: the gradient of the cost function
%Input:
%theta: the current coefficients of the classifier
%dataTrain: the data for training
%labelTrain: the labels for training
%lambda: the regularization parameter

function [cost,grad] = LRcostfun(theta,dataTrain,labelTrain,lambda)

% Initialize some useful values
num_dataTrain = length(labelTrain); % number of training examples


%Calculate cost
sigmoidValue = 1.0 ./ (1.0 + exp(-dataTrain*theta));

probability = sum(labelTrain.*log(sigmoidValue) + (1-labelTrain).*log(sigmoidValue))/num_dataTrain;

reg_term = theta.^2;
reg_term(1) = 0;

cost = probability + sum(lambda*reg_term)/(2*num_dataTrain);

%Calculate gradient^{
reg_term=theta*(lambda/num_dataTrain);
reg_term(1)=0;
grad=(dataTrain'*(sigmoidValue-labelTrain))*(1/num_dataTrain) + reg_term;



end