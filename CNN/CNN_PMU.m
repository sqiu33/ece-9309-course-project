%Training a CNN network to recognize the Estern Arabic Numbers
clc;clear;close all
%% Load and Explore Image data

% Load the digit sample data as an image datastore
digitDatasetPath = fullfile('C:\','Shuwei','Courses','Machine Learning','CourseProject',...
    'datasets','EasternArabicNums','PMU-UD');

imds = imageDatastore(digitDatasetPath,'IncludeSubfolders',true,'LabelSource','foldernames');

% % Display some of the images in the datastore
% figure;
% perm = randperm(length(imds.Files),20);
% for i = 1:20
%     subplot(4,5,i);
%     imshow(imds.Files{perm(i)});
% end

% Calculate the number of images in each category
labelCount = countEachLabel(imds);

% Get the size of the input image
img = readimage(imds,1);
sizeImg = size(img);

%% Specify Training and Validation Sets
%Let the training set contains 60% of all the images
[imdsTrain,imdsValidation] = splitEachLabel(imds,0.6,'randomize'); 


%% Define Network Architecture
layers = [
    imageInputLayer([sizeImg,1])
    convolution2dLayer(3,8,'Padding','same')%1st argument is filterSize; The 2nd is num of filters
    batchNormalizationLayer
    reluLayer
    
    maxPooling2dLayer(2,'Stride',2)
    
    convolution2dLayer(3,16,'Padding','same')
    batchNormalizationLayer
    reluLayer
    
    maxPooling2dLayer(2,'Stride',2)
    
    convolution2dLayer(3,32,'Padding','same')
    batchNormalizationLayer
    reluLayer
    
    fullyConnectedLayer(10)
    softmaxLayer
    classificationLayer];

%% Specify Training Options
options = trainingOptions('sgdm', ...
    'InitialLearnRate',0.01, ...
    'MaxEpochs',4, ...
    'Shuffle','every-epoch', ...
    'ValidationData',imdsValidation, ...
    'ValidationFrequency',30, ...
    'Verbose',false, ...
    'Plots','training-progress');

%% Train Network Using Training Data
net = trainNetwork(imdsTrain,layers,options);


%% Classify Validation Images and Compute Accuracy
YPred = classify(net,imdsValidation);
YValidation = imdsValidation.Labels;

accuracy = sum(YPred == YValidation)/numel(YValidation)