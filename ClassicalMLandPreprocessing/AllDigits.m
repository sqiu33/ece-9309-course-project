clear
clc
data = [];
dataBin = []; 
for i = 0:9
   strcat('train_BB_',num2str(i),'.csv')
   tr = csvread(strcat('train_BB_',num2str(i),'.csv'));
   data = [data;tr];
   trBin = csvread(strcat('train_BB_BW_',num2str(i),'.csv'));
   dataBin = [dataBin;trBin]; 
   
end

random_data = data(randperm(size(data, 1)), :);
random_dataBin = dataBin(randperm(size(dataBin,1)), :);
csvwrite('train_BB_inorder.csv', data); 
csvwrite('train_BB_BW_inorder.csv', dataBin); 
csvwrite('train_BB.csv',random_data); 
csvwrite('train_BB_BW.csv', random_dataBin); 
