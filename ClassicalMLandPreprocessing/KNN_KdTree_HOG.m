%%
clear 
close all
clc
%% 
imds = ReadInput(pwd); 
%% Specify Training and Validation Sets
%Let the training set contains 60% of all the images
[imdsTrain,imdsValidation] = splitEachLabel(imds,0.9,'randomize'); 

%% 
cellsize = [8,8];
train_features = []; 
for i = 1:length(imdsTrain.Files)
    
    img = readimage(imdsTrain, i);
    img = imbinarize(img);
    train_features(i,:) = extractHOGFeatures(img, 'CellSize', cellsize);
end
%%
test_features = []; 
for i = 1:length(imdsValidation.Files)
    
    img = readimage(imdsValidation, i);
    img = imbinarize(img); 
    test_features(i,:) = extractHOGFeatures(img, 'CellSize', cellsize);
end

%% 
X_train = train_features; 
Y_train = double(imdsTrain.Labels); 
X_test = test_features; 
Y_test = double(imdsValidation.Labels); 
%%
KdTreeMdl = fitcknn(X_train, Y_train, 'NSMethod', 'kdtree','NumNeighbors', 1);
rloss = resubLoss(KdTreeMdl)

% Cross-validation classifier
KdTreeCVMdl = crossval(KdTreeMdl); 
kloss_kdTr = kfoldLoss(KdTreeCVMdl)

labels = predict(KdTreeMdl, X_test); 

accuracy = sum(labels==Y_test)/length(Y_test)

