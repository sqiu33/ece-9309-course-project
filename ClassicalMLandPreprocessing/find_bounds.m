function [MAX_r, MAX_c, MIN_r, MIN_c] = find_bounds()

th = 100; 
[~, all_bin] = ReadJPG_Updated(th); 
indCont = (all_bin==0);
% figure
% 
% for i = 3300:size(all_bin,3)
%     imshow(all_bin(:,:,i),[0 1])
%     pause(0.002)
% end
    
for i = 1:size(all_bin,3)
    [id1, id2] = find(all_bin(:,:,i)==0);
    Max_r(i) = max(id1); Max_c(i) = max(id2); 
    Min_r(i) = min(id1); Min_c (i) = min(id2); 
end

MAX_r = max(Max_r); MAX_c = max(Max_c); 
MIN_r = min(Min_r); MIN_c = min(Min_c); 
