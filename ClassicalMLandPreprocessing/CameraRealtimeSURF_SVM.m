clear;clc;close all
imaqreset;%delete all image acquisition objects

% cam = videoinput('winvideo',1,'YUY2_2560x720');%create video input object
%  cam = webcam;
cam = videoinput('winvideo')
cam.ReturnedColorspace = 'grayscale';%Specify color space used in MATLAB
% preview(cam)
%To Trigger frames immediatelly - Set manual trigger
triggerconfig(cam,'manual');
cam.TriggerRepeat = Inf;
cam.FramesPerTrigger = 1;

%CameraCalibration;
% load('C:\Shuwei\CameraOnHand\stereoParams\stereoParams_KUKA_20180618.mat');

%Start the camera
start(cam)


% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % CNN Net Start
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % load('CNN_PMU_Net');
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % CNN_net = net;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % layers = CNN_net.Layers;
% layers(1) = imageInputLayer([480,640,3]);
load 'categoryClassifier_SVM_SURF.mat';
% cellSize = [8 8];

imagedim1 = 720;
imagedim2 = 1280;

%Trigger the camera to get one image
l = figure;
hold on;
while 1
    trigger(cam);
%     preview(cam)
%     pause(2)
    im = getdata(cam);
%     closepreview
    fCrop = im(imagedim1/2-150:imagedim1/2+150, imagedim2/2-250:imagedim2/2+250);
    fBW = double(fCrop>=120);
%     fBW = double(fCrop>=100);

%     imshow(fBW)
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %     %Separate a single stereo image into two images
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %     im1 = im(:,1:end/2);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %     im2 = im(:,end/2 + 1: end);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %     %Image rectification
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %     [imRe1,imRe2] = rectifyStereoImages(im1,im2,stereoParams);

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %     sz = CNN_net.Layers(1).InputSize;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %     imRe1Net = imresize(imRe1,sz(1:2)); 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %     imRe1Net = imresize(fBW,sz(1:2)); 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %     imRe1Net = imread('C:\Users\Asus-pc\OneDrive - The University of Western Ontario\Desktop\Parya\Parya Documents\4th Sem\Machine Learning\UCI-DATASET\1\1041.jpg');
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %     label1 = classify(CNN_net, imRe1Net);
%     label1 = predict(CNN_net, imRe1Net);
    fResize = imresize(fBW, [80 120]);
%     feature = extractHOGFeatures(fResize,'CellSize',cellSize);
    [label1, score] = predict(categoryClassifier, fResize);

    imshow(im);
    hold on;
     rectangle('Position', [imagedim2/2-250, imagedim1/2-150, 500, 300], ...
         'EdgeColor','red','LineWidth',3)
    text(10,20,num2str(label1-1),'Color','red','FontSize',48)
    drawnow;
     hold off
% % %     if 1+score(label1)<0.92
% % %             text(10,20,'Please Place the Paper Correctly.',...
% % %         'Color','black','FontSize',30);
% % %         drawnow
% % %         hold off
% % %     else
% % %     text(10,20,num2str(label1-1),'Color','red','FontSize',48);
% % %     text(10,100,strcat('Score:',num2str(1+round(score(label1),3))),'Color','blue','FontSize',30)
% % %     
% % %      drawnow;
% % %      hold off
% % %     end
end