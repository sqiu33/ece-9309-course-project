function im2CSV(number, allImages, all_bin)

allImages = permute(allImages,[2 1 3]); 
allImages = reshape(allImages, ...
    [1, size(allImages,1)*size(allImages,2), size(allImages,3)]);
allImages = permute(allImages,[3,2,1]);
allIm_wL = [number*ones(size(allImages,1),1) allImages]; % All 0 images with label
%%
all_bin = permute(all_bin,[2 1 3]); 
all_bin = reshape(all_bin, ...
    [1, size(all_bin,1)*size(all_bin,2), size(all_bin,3)]);
all_bin = permute(all_bin,[3,2,1]);
all0bin_wL = [number*ones(size(all_bin,1),1) all_bin]; % All 0 images with label
%%
csvwrite(strcat('train_BB_',num2str(number),'.csv'), allIm_wL); 
csvwrite(strcat('train_BB_BW_',num2str(number),'.csv'), all0bin_wL); 
