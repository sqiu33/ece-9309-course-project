function [all_img, all_img_bin] = ReadJPG_Updated(th)

digitDatasetPath = pwd;
imds = imageDatastore(digitDatasetPath,'IncludeSubfolders',true,'LabelSource','foldernames');

for i = 1:length(imds.Files)
    
    all_img(:,:,i) = readimage(imds, i); 
    

end

all_img_bin = double(all_img>=100); 
