function imds = ReadInput(dir)
digitDatasetPath = dir;
imds = imageDatastore(digitDatasetPath,'IncludeSubfolders',true,'LabelSource','foldernames');
end