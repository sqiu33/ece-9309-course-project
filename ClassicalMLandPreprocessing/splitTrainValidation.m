function [dsTrain, dsValidation] = splitTrainValidation(input, percentage)

inputRandom = input(randperm(size(input,1)),:); 
m = floor(size(input,1)*percentage); 

dsTrain = inputRandom(1:m,:); 
dsValidation = inputRandom(m+1:end,:); 

end